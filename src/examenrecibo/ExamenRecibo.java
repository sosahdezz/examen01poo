/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrecibo;

/**
 *
 * @author REDES
 */
public class ExamenRecibo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
       MetodosExamen examen = new MetodosExamen();
       examen.setNumRecibo(102);
       examen.setNombre("               Jose Lopez");
       examen.setDomicilio("        Av del Sol 1200");
       examen.setTipoServicio(1);
       examen.setCosto(2.00f);
       examen.setKilowats(450);
       examen.setFecha("            21 MARZO 2019");   
       
       
       System.out.println("El Num Recibo es:            " + examen.getNumRecibo());
       System.out.println("El Nombre es: " + examen.getNombre());
       System.out.println("El Num Domicilio es: " + examen.getDomicilio());
       System.out.println("El Tipo de Servicio es:      " + examen.getTipoServicio());
       System.out.println("El Costo es:                 " + examen.getCosto());
       System.out.println("Los Kilowatts Consumidos son: " + examen.getKilowats());
       System.out.println("La Fecha es: " + examen.getFecha());
       
       System.out.println("El Subtotal es:          " + examen.calcularSubtotal());
       System.out.println("El Impuesto es:          " + examen.calcularImpuesto());
       System.out.println("El Total a Pagar es:     " + examen.calcularTotal());
       
       System.out.println("     E J E M P L O  2  ");
       
       
       examen.setNumRecibo(103);
       examen.setNombre("    Abarrotes feliz");
       examen.setDomicilio(" Av del Sol");
       examen.setTipoServicio(2);
       examen.setCosto(3.00f);
       examen.setKilowats(1200);
       examen.setFecha("                21 MARZO 2019");   
       
       
       System.out.println("El Num Recibo es:         " + examen.getNumRecibo());
       System.out.println("El Nombre es:         " + examen.getNombre());
       System.out.println("El Num Domicilio es:     " + examen.getDomicilio());
       System.out.println("El Tipo de Servicio es:   " + examen.getTipoServicio());
       System.out.println("El Costo es:              " + examen.getCosto());
       System.out.println("Los Kilowatts Consumidos son: " + examen.getKilowats());
       System.out.println("La Fecha es:   " + examen.getFecha());
       
       System.out.println("El Subtotal es:          " + examen.calcularSubtotal());
       System.out.println("El Impuesto es:          " + examen.calcularImpuesto());
       System.out.println("El Total a Pagar es:     " + examen.calcularTotal());
       
       
       }
   
      
} 